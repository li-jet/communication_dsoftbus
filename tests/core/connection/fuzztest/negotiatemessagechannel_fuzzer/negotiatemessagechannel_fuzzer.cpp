/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "negotiatemessagechannel_fuzzer.h"
#include "comm_log.h"
#include "data/link_info.h"
#include "data/negotiate_message.h"
#include "protocol/wifi_direct_protocol.h"
#include "protocol/wifi_direct_protocol_factory.h"
#include <securec.h>

namespace OHOS::SoftBus {
const uint8_t *g_baseFuzzData = nullptr;
size_t g_baseFuzzSize = 0;
size_t g_baseFuzzPos;

template <class T>
T GetData()
{
    T objetct {};
    size_t objetctSize = sizeof(objetct);
    if (g_baseFuzzData == nullptr || objetctSize > g_baseFuzzSize - g_baseFuzzPos) {
        COMM_LOGE(COMM_TEST, "data Invalid");
        return objetct;
    }
    errno_t ret = memcpy_s(&objetct, objetctSize, g_baseFuzzData + g_baseFuzzPos, objetctSize);
    if (ret != EOK) {
        COMM_LOGE(COMM_TEST, "memcpy err");
        return {};
    }
    g_baseFuzzPos += objetctSize;
    return objetct;
}

void UnmarshallingFuzzTest(const uint8_t *data, size_t size)
{
    if (data == nullptr || size < sizeof(int32_t)) {
        COMM_LOGE(COMM_TEST, "Invalid param");
        return;
    }
	size_t chn = 5180;
    NegotiateMessage msg1;
    msg1.SetSessionId(1);
    msg1.SetIsModeStrict(true);
    msg1.SetMessageType(NegotiateMessageType::CMD_CONN_V2_REQ_1);
    msg1.SetIpv4InfoArray({ Ipv4Info("172.30.1.1"), Ipv4Info("172.30.2.1") });
    LinkInfo linkInfo1;
    linkInfo1.SetCenter20M(chn);
    linkInfo1.SetLocalBaseMac("01:02:03:04:05:06");
    msg1.SetLinkInfo(linkInfo1);

    auto protocol1 = WifiDirectProtocolFactory::CreateProtocol(ProtocolType::TLV);
    protocol1->SetFormat({ TlvProtocol::TLV_TAG_SIZE, TlvProtocol::TLV_LENGTH_SIZE2 });
    std::vector<uint8_t> output;
    msg1.Marshalling(*protocol1, output);

    NegotiateMessage msg2;
    auto protocol2 = WifiDirectProtocolFactory::CreateProtocol(ProtocolType::TLV);
    protocol2->SetFormat({ TlvProtocol::TLV_TAG_SIZE, TlvProtocol::TLV_LENGTH_SIZE2 });
    msg2.Unmarshalling(*protocol2, output);

    LinkInfo linkInfo2 = msg2.GetLinkInfo();

    std::vector<Ipv4Info> ipv4Array1 = msg1.GetIpv4InfoArray();
    std::vector<Ipv4Info> ipv4Array2 = msg2.GetIpv4InfoArray();

    NegotiateMessage msg3;
    auto protocol3 = WifiDirectProtocolFactory::CreateProtocol(ProtocolType::TLV);
    protocol1->SetFormat({ TlvProtocol::TLV_TAG_SIZE, TlvProtocol::TLV_LENGTH_SIZE2 });
    std::vector<uint8_t> output3;
    msg3.Marshalling(*protocol3, output3);

    NegotiateMessage msg4;
    auto protocol4 = WifiDirectProtocolFactory::CreateProtocol(ProtocolType::TLV);
    protocol2->SetFormat({ TlvProtocol::TLV_TAG_SIZE, TlvProtocol::TLV_LENGTH_SIZE2 });
    msg4.Unmarshalling(*protocol4, output3);
}
} // namespace OHOS::SoftBus

/* Fuzzer entry point */
extern "C" int32_t LLVMFuzzerTestOneInput(const uint8_t *data, size_t size)
{
    /* Run your code on data */
    OHOS::SoftBus::UnmarshallingFuzzTest(data, size);
    return 0;
}